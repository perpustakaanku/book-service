import mongoengine.errors

from routes.scheme import ResponseBooksSchema
from mongoengine import connect, disconnect
from mongoengine import Document, StringField, IntField, DateField
from config import load_conf
from fastapi import HTTPException, status


class books(Document):
    title = StringField(required=True)
    authors = StringField(required=True)
    isbn = StringField(required=True)
    isbn13 = StringField(required=True)
    language_code = StringField(required=True)
    num_pages = IntField(required=True)
    publication_date = DateField(required=True)
    publisher = StringField(required=True)
    counts = IntField(required=True)


def connect_database():
    config = load_conf()["database"]

    if config["username"] and config["password"]:
        __connection = connect(db=config["name"],
                               host=config["host"],
                               port=config["port"],
                               username=config["username"],
                               password=config["password"])
    else:
        __connection = connect(db=config["name"],
                               host=config["host"],
                               port=config["port"])

    if not __connection:
        raise Exception("Database Connection Failed")


def __build_book_dict(data):
    return {
        "id": str(data.pk),
        "title": data.title,
        "authors": data.authors,
        "counts": data.counts,
        "isbn": data.isbn,
        "isbn13": data.isbn13,
        "language_code": data.language_code,
        "num_pages": data.num_pages,
        "publication_date": data.publication_date,
        "publisher": data.publisher,
    }


def get_all_books(limit=5, offset=0):
    result = []

    connect_database()

    for i in books.objects[offset:offset + limit]:
        result.append(ResponseBooksSchema(**__build_book_dict(i)))

    disconnect()

    return result


def search_books(query: dict, limit=5, offset=0):
    result = []

    connect_database()
    for i in books.objects(**query)[offset: offset + limit]:
        result.append(ResponseBooksSchema(**__build_book_dict(i)))

    disconnect()
    return result


def add_book(**data):
    connect_database()
    books(**data).save()
    disconnect()

    return True


def get_book_by_id(id: str):
    connect_database()
    try:
        data = books.objects.get(id=id)
        result = ResponseBooksSchema(**__build_book_dict(data))
    except mongoengine.errors.ValidationError:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Book id not found")
    disconnect()

    return result


def delete_book_by_id(id: str):
    connect_database()
    try:
        books.objects.get(id=id).delete()
    except mongoengine.errors.ValidationError:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Book id not found")
    except mongoengine.errors.DoesNotExist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Book id not found")
    disconnect()
    return True


def edit_book_by_id(id: str, **data):
    connect_database()

    try:
        book_obj = books.objects.get(id=id)
        for i in data:
            setattr(book_obj, i, data[i])

        book_obj.save()
    except mongoengine.errors.ValidationError:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Book id not found")
    except mongoengine.errors.DoesNotExist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Book id not found")

    disconnect()

    return True
