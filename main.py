from models.books import get_all_books
from config import load_conf
from fastapi import FastAPI, Depends
from fastapi.security import HTTPBearer
from routes.get_books import get_all_books_route, search_books_route, get_book_by_id
from routes.manipulation import post_add_book, put_edit_book, delete_book_routes
from routes.scheme import *
import uvicorn

app = FastAPI()
oauth_scheme = HTTPBearer()


@app.get("/")
async def main_route():
    return {"message": "Nothing to do.", "status": 0}


@app.get("/books", response_model=list[ResponseBooksSchema])
async def get_books(data: BasicSelectingSchema):
    return await get_all_books_route(data)


@app.get("/search", response_model=list[ResponseBooksSchema])
async def search_books(data: SearchSchema):
    return await search_books_route(data)


@app.get("/book/{id}", response_model=ResponseBooksSchema)
async def get_book(id: str):
    return get_book_by_id(id)


@app.post("/book/add", response_model=ResponseStatus)
async def add_book(data: BooksSchema, token=Depends(oauth_scheme)):
    return post_add_book(token=token.credentials, book_data=data)


@app.put("/book/{id}")
async def edit_book(data: BooksSchema, id: str, token=Depends(oauth_scheme)):
    return put_edit_book(token=token.credentials, id=id, book_data=data)


@app.delete("/book/{id}")
async def delete_book(id: str, token=Depends(oauth_scheme)):
    return delete_book_routes(id=id, token=token.credentials)


if __name__ == "__main__":
    config = load_conf()["server"]
    uvicorn.run(app, **config)
