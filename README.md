# Book Service
Service ini berfungsi untuk melakukan proses CRUD yang berkaitan dengan pengelolaan buku pada perpustakaan.

## Mengenai Aplikasi Perpustakaan
Aplikasi yang saya buat adalah aplikasi management perpustakaan. Pada aplikasi ini berisi beberapa fitur yang 
telah diimplementasikan sebagai backend diantaranya adalah sebagai berikut:
* Akun pengguna
* Manajemen buku

## File Konfigurasi
Pada aplikasi ini terdapat file `config.json`. File ini berisikan konfigurasi server untuk menyambungkan ke database
atau service lain. Pada service ini, terdapat tiga konfigurasi penting, yaitu sebagai berikut:

### Server
Konfigurasi ini berisi mengenai bagaimana service ini akan dijalankan. 
* Nilai `host` diisikan dengan alamat IP yang akan
diattach sebagai server
* Nilai `port` adalah nilai port yang digunakan.

### Database
Konfigurasi ini berisi mengenai lokasi dan juga autentikasi database mongodb yang akan digunakan.

**Notes :** Berikan nilai `null` pada `username` dan `password` bila tidak menggunakannya.

### Auth
Settingan ini berisi mengenai lokasi tempat Auth Service bekerja.

## Cara Menjalankan Service
Berikut ini adalah langkah untuk menggunakan aplikasi ini:
1. Pastikan anda telah mengunduh Python terbaru dan telah menjalankan service Auth.
2. Buatlah lingkungan virtual Python jika dibutuhkan
3. Install _depedencies_ dari service ini. Gunakan :
```shell
pip install -r requirement.txt
```
4. Jalankan perintah erikut untuk memulai service ini:
```shell
python main.py
```

## Service Endpoint
Penjelasan mengenai endpoint dapat anda lihat disini:

[https://documenter.getpostman.com/view/9498435/U16krQv4](https://documenter.getpostman.com/view/9498435/U16krQv4)