from models.books import add_book, delete_book_by_id, edit_book_by_id
from routes.scheme import *
from fastapi import HTTPException, status
from config import load_conf
import requests

# CONSTANT
ADMIN_ROLE = 0b1000
DELETE_ROLE = 0b0100
CREATE_ROLE = 0b0010
EDIT_ROLE = 0b0001


def is_authorized_api(token, level):
    try:
        url = load_conf()["auth"]["url"] + "/is-authorized"
        param = {
            "action": level
        }

        r = requests.get(url=url, params=param, headers={
            "Authorization": f"Bearer {token}"
        })
        result = r.json()
    except:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Couldn't authorize your token to the API Server")

    if r.status_code == status.HTTP_401_UNAUTHORIZED:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Action is not allowed")
    else:
        return result["result"]


def is_authorized(token, level):
    return is_authorized_api(token, level) or is_authorized(token, ADMIN_ROLE)


def post_add_book(token: str, book_data: BooksSchema):
    if is_authorized(token, CREATE_ROLE):
        result = add_book(**book_data.dict())
        return ResponseStatus(status=result)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Action is not allowed")


def put_edit_book(token: str, id: str, book_data: BooksSchema):
    if is_authorized(token, EDIT_ROLE):
        result = edit_book_by_id(id=id, **book_data.dict())
        return ResponseStatus(status=result)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Action is not allowed")


def delete_book_routes(token: str, id: str):
    if is_authorized(token, DELETE_ROLE):
        result = delete_book_by_id(id)
        return ResponseStatus(status=result)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Action is not allowed")
