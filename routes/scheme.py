from pydantic import BaseModel
from datetime import date
from typing import Optional


class BooksSchema(BaseModel):
    title: str
    authors: str
    isbn: str
    isbn13: str
    language_code: str
    num_pages: int
    publication_date: date
    publisher: str
    counts: int


class BasicSelectingSchema(BaseModel):
    limit: int
    offset: int


class ResponseBooksSchema(BooksSchema):
    id: str


class SearchSchema(BasicSelectingSchema):
    authors: Optional[str] = None
    title: Optional[str] = None
    publisher: Optional[str] = None


class ResponseStatus(BaseModel):
    status: bool
