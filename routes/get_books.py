from models.books import get_all_books, search_books, get_book_by_id
from routes.scheme import *
from fastapi import HTTPException, status
import re


async def get_all_books_route(data: BasicSelectingSchema) \
        -> list[ResponseBooksSchema]:
    return get_all_books(data.limit, data.offset)


async def search_books_route(data: SearchSchema) -> list[ResponseBooksSchema]:
    query = dict()

    if data.authors:
        query["authors"] = re.compile(data.authors, re.IGNORECASE)

    if data.title:
        query["title"] = re.compile(data.title, re.IGNORECASE)

    if data.publisher:
        query["publisher"] = re.compile(data.publisher, re.IGNORECASE)

    if len(query) == 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="No any queries attached.")

    return search_books(query=query, limit=data.limit, offset=data.offset)


async def get_books_by_id_route(id: str):
    res = get_book_by_id(id)

    return res